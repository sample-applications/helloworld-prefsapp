/* * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <gtk/gtk.h>
#include "helloworld-prefs-app.h"
#include "helloworld-prefs-window.h"

#define WIN_WIDTH  480
#define WIN_HEIGHT 800

struct _HlwPrefsAppWindow
{
  GtkApplicationWindow parent;
  GtkWidget *button, *button_box;
  GSettings *settings;
};

G_DEFINE_TYPE(HlwPrefsAppWindow, hlw_prefs_app_window, GTK_TYPE_APPLICATION_WINDOW);

static void
button_pressed_cb (GtkWidget *button,
                   gpointer user_data)
{
  const gchar *to_be_button_status;
  g_autofree gchar *last_button_status = NULL;

  HlwPrefsAppWindow *self = HLW_PREFS_APP_WINDOW (user_data);

  g_message ("Clicked Center button");
  
  last_button_status = g_settings_get_string (self->settings, "button-status");

  /* For the reason of simplicity, the button action switches 
   * color between "ON" and "OFF".*/
 
  if (g_strcmp0 (last_button_status, "ON") == 0)
    {
      to_be_button_status = "OFF";
    }
  else
    {
      to_be_button_status = "ON";
    }

  if (!g_settings_set_string (self->settings, "button-status", to_be_button_status))
    {
      g_message ("The key, button-status, isn't writable.");
    }

  g_settings_sync ();

  gtk_button_set_label (GTK_BUTTON (self->button), to_be_button_status);
}


static void
hlw_prefs_app_window_init (HlwPrefsAppWindow *self)
{
  g_autofree gchar *last_button_status = NULL;
  
  HlwPrefsAppWindow *win = HLW_PREFS_APP_WINDOW(self);

  /* Create GSettings instance */
  win->settings = g_settings_new ("org.apertis.HelloWorldPrefsApp");
  
  /* Set button status from preference data */
  last_button_status = g_settings_get_string (win->settings, "button-status");
  
  g_message ("button_status from GSettings: '%s'.", last_button_status);

  gtk_window_set_default_size(GTK_WINDOW(win),WIN_WIDTH,WIN_HEIGHT);

  win->button = gtk_button_new();

  gtk_button_set_label(GTK_BUTTON(win->button),last_button_status);

  win->button_box = gtk_button_box_new (GTK_ORIENTATION_HORIZONTAL);

  gtk_container_add (GTK_CONTAINER(win->button_box), win->button);

  gtk_container_add (GTK_CONTAINER(win), win->button_box);

  g_signal_connect (self->button,
      "clicked", G_CALLBACK (button_pressed_cb),
      self);
  
  gtk_widget_show_all(GTK_WIDGET(win));
}

static void
hlw_prefs_app_window_class_init (HlwPrefsAppWindowClass *class)
{
}

HlwPrefsAppWindow *
hlw_prefs_app_window_new (HlwPrefsApp *app)
{
  return g_object_new (HLW_PREFS_APP_WINDOW_TYPE, "application", app, NULL);
}

