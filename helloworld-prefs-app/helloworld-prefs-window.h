/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __HELLOWORLD_PREFS_WINDOW_H__
#define __HELLOWORLD_PREFS_WINDOW_H__

#include <glib-object.h>
#include <gio/gio.h>
#include <gtk/gtk.h>
#include "helloworld-prefs-app.h"

G_BEGIN_DECLS

#define HLW_PREFS_APP_WINDOW_TYPE (hlw_prefs_app_window_get_type ())
G_DECLARE_FINAL_TYPE (HlwPrefsAppWindow, hlw_prefs_app_window, HLW_PREFS, APP_WINDOW, GtkApplicationWindow)
HlwPrefsAppWindow *hlw_prefs_app_window_new (HlwPrefsApp *app);

G_END_DECLS

#endif /* __HELLOWORLD_APP_H__*/
