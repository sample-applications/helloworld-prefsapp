/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"
#include "helloworld-prefs-window.h"
#include "helloworld-prefs-app.h"
#include <gtk/gtk.h>

struct _HlwPrefsApp
{
  GtkApplication parent;
};

G_DEFINE_TYPE (HlwPrefsApp, hlw_prefs_app, GTK_TYPE_APPLICATION);

static void 
hlw_prefs_app_activate(GApplication *app)
{
  HlwPrefsAppWindow *win;
  win = hlw_prefs_app_window_new(HLW_PREFS_APP(app));
  gtk_window_present(GTK_WINDOW(win));
}

static void
hlw_prefs_app_class_init (HlwPrefsAppClass *class)
{
  G_APPLICATION_CLASS(class)->activate = hlw_prefs_app_activate;
}

static void
hlw_prefs_app_init (HlwPrefsApp *self)
{
}

HlwPrefsApp *
hlw_prefs_app_new (void)
{
  return g_object_new (HLW_PREFS_TYPE_APP,
                       "application-id", "org.apertis.HelloWorld.PrefsApp",
                       NULL);
}
